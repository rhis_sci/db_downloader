/*
 * query_builder.cpp
 *
 *  Created on: Apr 6, 2020
 *      Author: jamil.zaman
 */
#include "query_builder.h"
#include <sstream> 
#include "configmanager.h"

query_builder::query_builder()
    : cm_(config_manager::instance("")),
      sql_table_map_ (cm_.sql_table_mapping_()),
      query_map_{}
{
    for (auto table : sql_table_map_) {
#ifdef DEBUG
        std::cout << "table '" << table.first << "' \tfile: '" << table.second << "'"  << std::endl;
#endif
        if(query_map_.find(table.second) == query_map_.end()) {
             read_queries(table.second, query_map_);
        }
    }
#ifdef DEBUG
    for (auto table : query_map_) {
        std::cout << "TABLE: '" << table.first << "' QUERY :\n "
                << table.second<< std::endl;
    }
#endif
}

query_builder::~query_builder() {}

const std::string query_builder::get_query(const std::string& table_name) {
    std::string table_query = "";
    auto table = sql_table_map_.find(table_name); //search file for the table
    if(table != sql_table_map_.end()) {
        auto query = query_map_.find(table->second); //get the query from file extracted
        if( query != query_map_.end()) {
            table_query = search_replace_all(query->second, "<table_name>", table_name);
        }
    }
    return table_query;
}

std::string query_builder::search_replace_all(const std::string& str, const std::string& search_str, const std::string& replace_str) {

    size_t search_pos = 0;
    std::string copy_str = str;
    //loop through the string to find all occurrence and replace them
    while((search_pos = copy_str.find(search_str, search_pos)) != std::string::npos) {
        copy_str.replace(search_pos, search_str.length(), replace_str.c_str(), replace_str.length());
    }
    return copy_str;
}

void query_builder::read_queries(const std::string& file_name, std::map<std::string, std::string>& query_map) {
    std::cout << "Reading file " << cm_.sql_directory() + file_name << std::endl;
    //add path before the file name
    std::ifstream ifs(cm_.sql_directory() + file_name);
    query_map[file_name] = std::move(std::string (std::istreambuf_iterator<char>{ifs}, {}));
}
