/*
 * configmanager.cpp
 *
 *  Created on: Sep 25, 2020
 *      Author: jamil.zaman
 */

#include "configmanager.h"
#include <fstream>
#include <sstream>
#include <vector>

using boost::property_tree::ptree;

void read(ptree const& pt, std::string& s) {
    s = pt.get_value(s);
}

void read(ptree const& pt, unsigned int& s) {
    s = pt.get_value(s);
}

//sample reading from property
void read(ptree const& pt, Object& object) {
    read(pt.get_child("port"), object.port);
    read(pt.get_child("address"), object.address);
    /*read(pt.get_child("property2"), object.property2);
    read(pt.get_child("property3"), object.property3);
    read(pt.get_child("property4"), object.property4);
    read(pt.get_child("property5"), object.property5);*/
}

template<typename T>
T read_as(ptree const& pt) {
    T v;
    read(pt, v);
    return v;
}

//there is no split function in the std c++98 or c++11 untill C++20
int split(std::string& str, std::vector<std::string>& tokens, char delim) {
    std::istringstream buf(str);
    std::string holder;
    while (std::getline(buf, holder, delim)) {
        tokens.push_back(holder);
    }
    return tokens.size();
}

config_manager::config_manager(const std::string& _config_file)
    : address_{""}, db_name_{""}, port_{0}, db_port_{0}, thread_multiplier_{1},
      max_connections_{10}, password_{""}, db_type_{""}, sql_directory_{""}, operation_mode_{""}, table_map_{} {
    //read the config file and load properties
    std::ifstream ifs(_config_file);
    std::istringstream iss(std::string(std::istreambuf_iterator<char>{ifs},{}));
    boost::property_tree::ptree config;
    try {
        read_ini(iss, config);

        std::cout << "Database Server " << config.get<std::string>("database.address") << std::endl;
        std::cout << "Database Port " <<   config.get<int>("database.port") << std::endl;
        address_ = config.get<std::string>("database.address");
        db_port_ = config.get<int>("database.port");
        db_name_ = config.get<std::string>("database.name");
        password_ = config.get<std::string>("database.password");
        user_ = config.get<std::string>("database.user");
        db_type_ = config.get<std::string>("database.type");
        port_ = config.get<int>("application.port");
        thread_multiplier_ = config.get<int>("application.thread_multiplier");
        max_connections_ = config.get<int>("application.max_connections");
        sql_directory_ = config.get<std::string>("application.sql_directory");
        operation_mode_ = config.get<std::string>("application.operation_mode");

        //load the mapping of the sql
        for(auto it : config.get_child("data_table")) {
#ifdef DEBUG
            std::cout << "Key " << it.first << " value " << it.second.get_value<std::string>() << std::endl;
#endif
            table_map_[it.first] = it.second.get_value<std::string>();
        }


    }catch (const std::exception &e) {
        std::cerr <<"ERROR OCCURRED LOADING CONFIGURATION" << std::endl;
        std::cerr << e.what() << std::endl;
    }
}

config_manager::~config_manager() {
    // TODO Auto-generated destructor stub
}

