/*
 * networkmanager.cpp
 *
 *  Created on: Apr 28, 2020
 *      Author: jamil.zaman
 */

#include <iostream>
#include <algorithm>
#include <thread>
#include <future>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "networkmanager.h"
#include "clientmanager.h"
#include "time_wrapper.h"
#include "configmanager.h"

#ifdef MCHECK
#include <mcheck.h>
#endif

const int max_length = 1024;

network_manager::network_manager(short _port, std::string &_sq_database)
: port_(_port),
  sq_database_(_sq_database),
  c_time_(time_wrapper::get_timer())
{
    // TODO Auto-generated constructor stub
}

network_manager::~network_manager() {
    // TODO Auto-generated destructor stub
}

void network_manager::server_loop()
{
#ifdef MCHECK
//mtrace();
#endif
    tcp::acceptor a(io_service, tcp::endpoint(tcp::v4(), port_));
    //TODO: why raw pointer
    std::map<client_ptr, std::future<int>> results;
    std::vector<std::future<int>> client_cleaner;

#ifdef MCHECK
    for (int i = 0 ;i < 2; i++) //forever, server loop may need to install a signal to stop it gracefully
#else
    for (;;) //forever, server loop may need to install a signal to stop it gracefully
#endif
    {
        socket_ptr sock(new tcp::socket(io_service));
        auto cm (std::make_shared<client_handler>(sock, sq_database_));
        client_list.push_back(cm);

        a.accept(*sock);
        //TODO:Welcome client
        //check for closed connections and remove them
        std::cout << c_time_<< "TOTAL CONNECTED CLIENT: " << client_list.size() << " FUTURES: " << results.size() << std::endl;
        if(client_list.size() > config_manager::instance("").max_connections()) {
            results[cm] = std::async(std::launch::async, &client_handler::clean_client, cm, true);
        } else {
            results[cm] = std::async(std::launch::async, &client_handler::session, cm);
        }

        client_cleaner.push_back(std::async(std::launch::async, &network_manager::remove_dead_clients, this, std::ref(results)));

        client_cleaner.erase(
            std::remove_if(
                client_cleaner.begin(), client_cleaner.end(),
                [] (decltype (*client_cleaner.begin()) &x) {
                    return x.wait_for(std::chrono::milliseconds(0)) == std::future_status::ready;
                }
            ),
            client_cleaner.end()
        );
    }
}

int network_manager::remove_dead_clients(std::map<client_ptr, std::future<int>>& _results) {
    //remove inactive clients
    for(auto clients = _results.begin(); clients != _results.end();) {
        if (
            clients->first->get_status() == client_handler::DONE ||
            clients->first->get_status() == client_handler::ERROR
#ifdef MCHECK
            || clients->first->get_status() == client_handler::INIT
#endif 
        ) {
            clients->second.get(); //finish the async task
            _results.erase(clients++); //post iteration is important,
        } else if (clients->first->get_status() == client_handler::TIMEOUT) {
            //TIMEDOUT socket do not close by themselves and needs active shutdown
            boost::system::error_code error{};
                //TODO: Should we check for error
                clients->first->get_socket()->shutdown(
                        boost::asio::ip::tcp::socket::shutdown_both, error);
                clients->first->get_socket()->close();
                clients->second.get(); //finish the async task//finish the async task
                _results.erase(clients++); //post iteration is important,
        } else {
            ++clients;
        }
    }

    client_list.erase(
        std::remove_if(
            client_list.begin(), client_list.end(),
            [](decltype (*client_list.begin()) &x) { //lamda - wish I could use auto
                return !x->is_alive();
            }
        ),
        client_list.end()
    );
    return 0;
}

