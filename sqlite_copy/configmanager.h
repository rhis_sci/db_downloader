/*
 * configmanager.h
 *
 *  Created on: Sep 25, 2020
 *      Author: jamil.zaman
 */

#ifndef CONFIGMANAGER_H_
#define CONFIGMANAGER_H_

#include <iostream>
#include <map>
#include <boost/property_tree/ini_parser.hpp>

static std::string const sample = R"(
[BLA]
properties1=val1, val2, val3
property2=Blah
property3=Blah
property4=Blah
property5=Bla bla bla

[BLO]
properties1=val1
property2=Blah
property3=Blah
property4=Blah
property5=Bla bla bla
)";

struct Object {
    std::string address;
    std::string password;
    unsigned int port;

};

class config_manager {
private:
    config_manager(const std::string& _conf); // Disallow instantiation outside of the class.
public:
    config_manager(const config_manager&) = delete;
    config_manager& operator=(const config_manager &) = delete;
    config_manager(config_manager &&) = delete;
    config_manager & operator=(config_manager &&) = delete;
    const unsigned int& port() {return port_;};
    const unsigned int& db_port() {return db_port_;};
    const std::string& db_name() {return db_name_;};
    const unsigned int& thread_multiplier() {return thread_multiplier_;};
    const unsigned int& max_connections() {return max_connections_;};
    const std::string& address() const {return address_;};
    const std::string& password() const {return password_;};
    const std::string& user() const {return user_;};
    const std::string& sql_directory() const {return sql_directory_;};
    const std::string& operation_mode() const {return operation_mode_;};
    const std::map<std::string, std::string>& sql_table_mapping_() const {return table_map_;};
    const bool is_district_mode() const {return operation_mode_.compare("zilla") == 0; }

    static config_manager& instance(const std::string& _conf) {
        static config_manager config(_conf);
        return config;
    }
public:
    //config_manager();
    virtual ~config_manager();
    //static const config_manager&  get_config();
private:
    std::string address_;
    std::string db_name_;
    unsigned int port_;
    unsigned int db_port_;
    unsigned int thread_multiplier_;
    unsigned int max_connections_;
    std::string password_;
    std::string user_;
    std::string db_type_;
    std::string sql_directory_;
    std::string operation_mode_;
    std::map<std::string, std::string> table_map_;
};

#endif /* CONFIGMANAGER_H_ */
